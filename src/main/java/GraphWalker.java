import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * @author Evgeny Pavlovsky
 */
public class GraphWalker {

  private Listener listener;

  public GraphWalker(Listener listener) {
    this.listener = listener;
  }

  public void walk(Map<String, Object> rootDictionary) {

    //that stack is actually a replacement for your recursion
    //with recursion you use system stack for your business logic
    //and that is not a good idea
    final Stack<Object> nodesToVisit = new Stack<>();

    nodesToVisit.add(rootDictionary);

    while (!nodesToVisit.isEmpty()) {

      Object node = nodesToVisit.pop();

      //that is what we are looking for - calling listener
      if (isList(node)) {
        listener.onList((List<Object>) node);
      }

      //adding child non-leaf nodes to stack for further visiting
      List<Object> children = getChildren(node);
      for (Object child : children) {
        if (isNonLeafNode(child)) {
          nodesToVisit.push(child);
        }
      }
    }
  }

  //just getting children out of node, nothing special at all
  private List<Object> getChildren(Object node) {
    if (isList(node)) {
      return (List<Object>) node;
    } else if (isDictionary(node)){
      return new ArrayList<Object>(((Map)node).values());
    }

    return new ArrayList<>();
  }

  private boolean isDictionary(Object value) {
    return value instanceof Map;
  }

  private boolean isList(Object value) {
    return value instanceof List;
  }

  private boolean isNonLeafNode(Object value) {
    return isDictionary(value) || isList(value);
  }
}
