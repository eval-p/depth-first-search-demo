import java.util.ArrayList;
import java.util.List;

/**
 * @author Evgeny Pavlovsky
 */
public class Listener {

  private List<String> result = new ArrayList<>();

  public void onList(List<Object> items) {
    //do whatever you want here

    //for example I will just make a string from each list
    StringBuilder currentString = convertToString(items);
    result.add(currentString.toString());
  }

  List<String> getResult() {
    return result;
  }

  private StringBuilder convertToString(List<Object> items) {
    final StringBuilder currentString = new StringBuilder();
    for (Object item : items) {

      if (item instanceof String) {
        currentString.append(item.toString()).append(" ");
      }
    }
    return currentString;
  }
}
