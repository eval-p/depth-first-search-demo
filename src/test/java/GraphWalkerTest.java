import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Evgeny Pavlovsky
 */
public class GraphWalkerTest {

  @Test
  public void callsListenerForEachList() throws IOException {
    final Listener listener = new Listener();
    final GraphWalker walker = new GraphWalker(listener);
    Map<String, Object> topLevelDict = readFromJson();

    walker.walk(topLevelDict);

    List<String> result = listener.getResult();
    assertThat(result).hasSize(9);
    assertThat(result).contains("very simple list ");
    assertThat(result).contains("another simple list ");
    assertThat(result).contains("not a simple list ");
    assertThat(result).contains("one more simple list ");
    assertThat(result).contains("another not simple list ");
    assertThat(result).contains("almost last not simple list ");
    assertThat(result).contains("last not simple list ");
    assertThat(result).contains("last simple list ");
    assertThat(result).contains("the most deeply placed list ");
  }

  //just a utility method to read JSON file with test data
  private Map<String, Object> readFromJson() throws IOException {
    InputStream stream = getClass().getResourceAsStream("/graph-walker-test.json");
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(stream, new TypeReference<Map<String, Object>>(){});
  }
}
