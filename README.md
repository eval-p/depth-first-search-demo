### Depth First Search without recursion

**GraphWalker** - iterates through the graph and calls Listener for each found List (call it array if you want).

**Listener** - can contain any logic you like. I was making a simple string from array and collecting them.

**GraphWalkerTest** - simple test that loads data from graph-walker-test.json file, calls walker and checks the result that was collected by listener.
  

The main benefit comparing to recursion - no usage of system stack. If you need a stack for your business logic - you should create your own stack for it.
With your own stack you can control it's size so you will never run out of memory.